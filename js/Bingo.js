import {pubsub} from './pubsub.js';
import './components/bingo-ball/bingo-ball.js';

export class Bingo {
    constructor(maxNum){
        this.maxNum = maxNum;
        this.maxLengthLastestNumbers = 5;
        this.lastestsNumbers = [];
        this.buttonSacar = document.getElementById("sacarBt");
        this.buttonReiniciar = document.getElementById("reiniciarBt");
        this.divNum = document.getElementsByClassName("number-container")[0];
        this.divLastNum = document.getElementsByClassName("last-numbers")[0];
        this.divTotalOfBalls = document.getElementById("total-of-balls");

        this.initialize();

        this.buttonSacar.addEventListener("click", () => {
            this.getNewBall();
        });

        this.buttonReiniciar.addEventListener("click", () => {
            this.initialize();
        });
    }

    initialize(){
        this.divNum.innerText = "";
        this.divLastNum.innerText = "";
        this.divTotalOfBalls.innerText = "";
        this.numObtainedList = [];
        this.lastestsNumbers = [];
        let bingoBallsContainer = document.getElementsByTagName("bingo-balls-container")[0];
        if (bingoBallsContainer){
          bingoBallsContainer.parentNode.removeChild(bingoBallsContainer);
        }
        this.createBallsContainer(this.maxNum);
    }

    getNewBall(){
        if (!this.allBallsAreOut()){
            let obtainedNumber = this.obtainNewNumber();
            if(this.numObtainedList.includes(obtainedNumber)){
                this.getNewBall();
                return;
            }

            this.divNum.innerText = obtainedNumber;
            this.numObtainedList.push(obtainedNumber);
            this.divLastNum.innerHTML = this.lastestsNumbers.join("&nbsp;&nbsp;&nbsp;");
            this.lastestsNumbers.push(obtainedNumber);
            if (this.lastestsNumbers.length == this.maxLengthLastestNumbers+1 ){
                this.lastestsNumbers.shift();
            }

            this.divTotalOfBalls.innerText = `Bola: ${this.numObtainedList.length} / ${this.maxNum}`;

            pubsub.publish('ballAdded', obtainedNumber);
        } else {
            alert("¡El juego a terminado!");
        }
    }

    allBallsAreOut(){
        return this.numObtainedList.length == this.maxNum;
    }

    obtainNewNumber(){
        return Math.floor(Math.random()*(this.maxNum) + 1);
    }

    createBallsContainer(maxBallNumber){
      let bingoBallsContainer = document.createElement("bingo-balls-container");
      bingoBallsContainer.setAttribute("maxBallNumber", this.maxNum);
      let elementWhereAddTo = document.getElementsByClassName("box balls-list")[0];
      elementWhereAddTo.insertBefore(bingoBallsContainer, this.buttonReiniciar);
    }
}
