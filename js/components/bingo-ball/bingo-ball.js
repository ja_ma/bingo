import {template} from "./bingo-ball-template.js";
import { pubsub } from '../../pubsub.js';

class BingoBall extends HTMLElement {
  constructor(){
    super();
  }

  get number() {
    return this.getAttribute("number");
  }

  set number(value){
    this.setAttribute("number", value);
  }

  get extracted(){
    return this.getAttribute("extracted");
  }

  set extracted(value){
    this.setAttribute("extracted", value);
  }

  connectedCallback(){
    this.render();
    pubsub.subscribe("ballAdded", (value) => this.updateBall(value));
  }

  render(){
    const root = this.attachShadow({mode: 'open'});
    root.appendChild(template.content.cloneNode(true));
    this.getElement(".ball-container").innerText = this.number;
  }

  getElement(elementSelector){
    return this.shadowRoot.querySelector(elementSelector);
  }

  updateBall(value){
    if (parseInt(value) == parseInt(this.number)) {
      this.extracted = true;
      this.getElement(".ball-container").classList.add("extracted");
    }
  }
}

customElements.define('bingo-ball', BingoBall);
