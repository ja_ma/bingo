export const template = document.createElement("template");

template.innerHTML = `
<style media="screen">
  .ball-container{
    color: black;
    background-color: lightblue;
    opacity: 0.5;
    font-size: 1em;
    line-height: 1.6em;
    height: 1.6em;
    width: 1.6em;
    margin: auto;
    border-radius: 50%;
    border: 1px solid black;
    transition: opacity 0.5s;
  }

  .extracted {
    background-color: green;
    color: white;
    opacity: 1;
  }
</style>

<div class="ball-container"></div>
`;
