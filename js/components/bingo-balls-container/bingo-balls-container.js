import {template} from "./bingo-balls-container-template.js";


class BingoBallsContainer extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback(){
    this.render();
  }

  get maxBallNumber(){
    return this.getAttribute("maxBallNumber")
  }

  render(){
    const root = this.attachShadow({mode: 'open'});
    root.appendChild(template.content.cloneNode(true));
    this.createBalls(this.maxBallNumber);
  }

  getElement(elementSelector){
    return this.shadowRoot.querySelector(elementSelector);
  }

  createBalls(maxBallNumber){
    let documentFragment = document.createDocumentFragment();
    for(let i=1; i <= maxBallNumber; i++) {
        let ball = document.createElement("bingo-ball");
        ball.setAttribute("number", i);
        documentFragment.appendChild(ball);
    }
    this.getElement("#balls-container").appendChild(documentFragment);
  }

}

customElements.define("bingo-balls-container", BingoBallsContainer)
