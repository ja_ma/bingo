export const template = document.createElement("template");

template.innerHTML = `
  <style>
   #balls-container {
     display: flex;
     flex-wrap: wrap;
   }

   bingo-ball {
     flex: 0 0 10%;
     margin-bottom: 10px;
   }
  </style>
  
  <div id="balls-container"></div>
`;
