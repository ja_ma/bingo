import {Bingo} from './Bingo.js';
import './components/bingo-balls-container/bingo-balls-container.js';

document.addEventListener("DOMContentLoaded", () => {
  let maxBallNumber = 90;
  let bingo = new Bingo(maxBallNumber);
});
