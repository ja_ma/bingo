## Objetivo

Sencilla aplicación que permite generar números aleatorios, del 1 al 90, para jugar al Bingo.

Muestra los últimos números generados, por orden de aparición, y una lista con todos los números, resaltando los extraídos.

## Demo

https://ja_ma.gitlab.io/bingo/
